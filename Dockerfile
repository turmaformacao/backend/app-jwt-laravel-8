FROM ubuntu:latest

ENV APP_HOME /var/www/html/
WORKDIR ${APP_HOME}

ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update && apt install -y \
    apache2 \
    php \
    php-common \
    php-fpm \
    php-pdo \
    php-opcache \
    php-zip \
    php-phar \
    php-iconv \
    php-curl \ 
    php-mbstring \
    php-fileinfo \
    php-json \
    php-xml \
    php-xmlwriter \
    php-simplexml \
    php-dom \
    php-mysql \
    curl iputils-ping

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN rm -rf composer-setup.php

COPY . ${APP_HOME}

RUN composer install --no-dev --optimize-autoloader
COPY ./infra/loja.conf /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod rewrite
RUN chmod -R 755 ${APP_HOME}/bootstrap ${APP_HOME}/storage
RUN chown -R www-data:www-data ${APP_HOME}/storage/

ENTRYPOINT [ "/usr/sbin/apachectl", "-D", "FOREGROUND" ]