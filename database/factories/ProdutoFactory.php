<?php

namespace Database\Factories;

use App\Models\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Produto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function gerarValor(): Float
    {
        $valor = rand(2, 9999);
        return $valor;
    }
    public function definition()
    {
        
        return [
            'nome' => explode(' ', $this->faker->name('male'))[1],
            'valor' => $this->gerarValor(),
            'descricao' => 'Lorem Ipsum is simply dummy text of the printing...',
            'quantidade' => rand(1, 10),
        ];
    }
}
