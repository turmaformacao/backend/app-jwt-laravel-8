<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Grupo;

class GrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupos = [
            'Administrador',
            'Cliente'
        ];

        foreach($grupos as $grupo) {
            Grupo::updateOrCreate([
                'nome' => $grupo
            ]);
        }
    }
}
