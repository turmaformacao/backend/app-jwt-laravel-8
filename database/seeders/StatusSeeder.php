<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            'Aguardando pagamento',
            'Em processamento',
            'Concluído'
        ];

        foreach($status as $item) {
            Status::updateOrCreate([
                'nome' => $item
            ]);
        }
        
    }
}
