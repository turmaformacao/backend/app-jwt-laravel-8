<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\ProdutoController;

class MeuPrimeiroTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testContarTotalProdutos()
    {
        // Cenário
        $produto = new ProdutoController();

        // Ação
        $total_produtos = count($produto->index()) ? true: false;

        // Validação
        $this->assertTrue($total_produtos);
        
    }

}
