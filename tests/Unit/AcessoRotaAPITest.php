<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AcessoRotaAPI extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEsperaReceberParametroIDemRotaGetApi()
    {
        // Cenário
        $id = 9999;
        $url = 'http://localhost:8000/api/phpunit/'. $id;
        
        // Ação
        $response  = $this->get($url);

        // https://auth0.com/blog/testing-laravel-apis-with-phpunit/
        // print_r($response->data);

        // Validação
        $this->assertEquals($response['data'], $id);
    }

    public function testEsperaEfetuarLogin()
    {
        // Cenário
        $payload = [
            'email' => 'kailyn88@example.net',
            'password' => 'password'
        ];
        $url = 'http://localhost:8000/api/login/';

        // Ação
        $response = $this->post($url, $payload);
        $response_decodificado = (array) json_decode($response->content());
        // dd($response_decodificado);
        // Validação
        $this->assertArrayHasKey('access_token', $response_decodificado);
    }
}