<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use App\Models\ProdutoVenda;

class Produto extends Model
{
    use HasFactory;

    public function getPedidos()
    {
        return response()->json($this->hasMany(ProdutoVenda::class));
    }
}
