<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Cadastro extends Model
{
    use HasFactory;

    static public function setCadastro($request)
    {
        return User::create($request->all());
    }
}
