<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cadastro;

class CadastroController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'password' => 'required|min:8',
        ]);
        
        if (!$validated) {
            return response()->json($validated);
        }

        return Cadastro::setCadastro($request);
    }
}
