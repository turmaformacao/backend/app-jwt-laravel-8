<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\VendaController;
use App\Http\Controllers\ProdutoVendaController;
use App\Http\Controllers\CadastroController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
    Rotas não autenticadas
*/
Route::group([
    'namespace' => 'App\Http\Controllers'
], function () {

    Route::resource('cadastro', 'CadastroController');
    Route::post('login', 'AuthController@login')->name('login');
    Route::resource('produto', 'ProdutoController');

    Route::get('naoAutorizado', function () {
        return response()->json(['data' => 'Não autenticado!'], 401);
    })->name('naoAutorizado');

    Route::get('/phpunit/{id?}', function ($id = null) {
        return response()->json(['data' => $id, 'mensagem' => 'Resposta em json!'], 200);
    });

});

/*
    Rotas autenticadas
*/
Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'App\Http\Controllers'
], function ($router) {

    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::resource('meusPedidos', 'ProdutoVendaController');
    Route::post('finalizarCompra', 'VendaController@store');
    Route::post('meusPedidos', 'VendaController@meusPedidos');

});
